package com.sunderc.eltropy.util;

import com.sunderc.eltropy.location.model.DirectionParams;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class GoogleDistanceCalculatorTestU {

    GoogleDistanceCalculator calculator = new GoogleDistanceCalculator();

    @Test
    public void testDistance() throws IOException {
           DirectionParams params = new DirectionParams(28.7041F,77.1025F,29.7041F,
                   77.1025F);

           params = calculator.calculateDistanceAndAddresses(params);
    }
}