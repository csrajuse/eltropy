CREATE TABLE IF NOT EXISTS ROLES (id serial PRIMARY KEY, name VARCHAR(50));
CREATE TABLE IF NOT EXISTS LOCATION (id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), latitude float, longitude float, address text,created_at timestamp);
CREATE TABLE IF NOT EXISTS USERS (id serial PRIMARY KEY, username varchar(255), password varchar(255),first_name VARCHAR(255), last_name VARCHAR(255), role bigint, current_location uuid ,active boolean,deleted boolean,FOREIGN KEY (role) REFERENCES roles(id),
FOREIGN KEY (current_location) REFERENCES location(id));
CREATE TABLE IF NOT EXISTS RIDES (id uuid PRIMARY KEY DEFAULT uuid_generate_v4() , time_requested TIMESTAMP, user_requested bigint NOT NULL,location uuid NOT NULL, to_location uuid NOT NULL,status VARCHAR(50) ,distance int,price float,FOREIGN  KEY (user_requested) REFERENCES USERS(id), FOREIGN KEY (location) REFERENCES LOCATION(id),FOREIGN KEY (to_location) REFERENCES LOCATION(id));
CREATE TABLE IF NOT EXISTS RIDES_ACCEPTED (id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), time_accepted TIMESTAMP, driver_user bigint NOT NULL, ride uuid, at_location uuid,status VARCHAR(50),FOREIGN KEY (driver_user) REFERENCES users(id),
FOREIGN KEY (at_location) REFERENCES location(id));
CREATE TABLE IF NOT EXISTS POTENTIAL_RIDES (id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), ride uuid,for_driver bigint NOT NULL,created_at timestamp, active boolean, 
FOREIGN KEY (ride) REFERENCES rides(id), FOREIGN KEY (for_driver) REFERENCES users(id)); 
CREATE TABLE IF NOT EXISTS ONGOING_RIDE_LOCATION (id uuid PRIMARY KEY DEFAULT uuid_generate_v4(), accepted_ride uuid,at_location uuid, created_at timestamp, FOREIGN KEY (accepted_ride) REFERENCES rides_accepted(id),
FOREIGN KEY (at_location) REFERENCES location(id));
CREATE TABLE DRIVER_PREFERENCES (id serial PRIMARY KEY, driver bigint,name VARCHAR(50), value VARCHAR(50), FOREIGN KEY (driver) REFERENCES USERS(id));
