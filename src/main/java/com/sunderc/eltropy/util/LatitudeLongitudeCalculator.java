package com.sunderc.eltropy.util;

/**
 * Utility class to calculate the Latitude and Longitude by the radius.
 *
 * Given the radius of the driver. The max/min latitude and max/min longitude values will returned.
 * These values will be used to add potential rides for a given the available rides for a driver.
 *
 * To simplify the calculation , for a value of 1 difference in latitude or longitude it is a 131 kms distance.
 * This is ignoring the curvature of the earth as a sphere so is an approximation.
 *
 * Assuming that is the calculation, we will be calculating the max/min latitude and max/min longitude based on the
 * distance sent.
 *
 * Here are the maximum and min values of India
 * min latitude 8.0772
 * max latitude 37.0
 * min longitude 68
 * max longitude 97
 */
public class LatitudeLongitudeCalculator {

    private static final Float MAX_LATITUDE = 37.0F;
    private static final Float MIN_LATITUDE = 8.0772F;
    private static final Float MAX_LONGITUDE = 97F;
    private static final Float MIN_LONGITUDE = 68F;

    /**
     * Does not mean all the values within the bounds are in India, as this just assumes a maximum oval that fits whole India
     * For more accurate calculation we need to be using google Map API
     * @return
     */
    public static boolean validateIfLocationValuesAreInIndia(Float latitude,Float longitude){
        if(latitude>MIN_LATITUDE && latitude<MAX_LATITUDE && longitude > MIN_LONGITUDE && longitude < MAX_LONGITUDE){
            return true;
        }
        return false;
    }

    /**
     *
     * @param distance in Kms
     * @param latitude
     * @param longitude
     * @return
     */
    public static MaxMinLatAndLong calculateMaxMinLatAndLongForIndia(int distance,Float latitude,Float longitude){
        if(!validateIfLocationValuesAreInIndia(latitude,longitude)){
            return null;
        }
        Float difference = (float)distance/131;
        Float maxLatitude = latitude+difference>MAX_LATITUDE ? MAX_LATITUDE : latitude+difference;
        Float minLatitude = latitude-difference>MIN_LATITUDE ? latitude-difference : MIN_LATITUDE;
        Float maxLongitude = longitude+difference>MAX_LONGITUDE ? MAX_LONGITUDE : longitude+difference;
        Float minLongitude = longitude-difference>MIN_LONGITUDE ? longitude-difference : MIN_LONGITUDE;

        return new MaxMinLatAndLong(minLatitude,maxLatitude,minLongitude,maxLongitude);
    }

    /**
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    public static double calculateDistanceBetweenCoordinates(Float lat1,Float lon1,Float lat2,Float lon2){
        double lat1Dob = Math.toRadians(lat1);
        double lon1Dob = Math.toRadians(lon1);
        double lat2Dob = Math.toRadians(lat2);
        double lon2Dob = Math.toRadians(lon2);

        double earthRadius = 6371.01; //Kilometers
        return earthRadius * Math.acos(Math.sin(lat1)*Math.sin(lat2) + Math.cos(lat1)*Math.cos(lat2)*Math.cos(lon1 - lon2));
    }

    public static class MaxMinLatAndLong{
        public Float minLatitude;
        public Float maxLatitude;
        public Float minLongitude;
        public Float maxLongitude;

        public MaxMinLatAndLong(Float minLatitude, Float maxLatitude, Float minLongitude, Float maxLongitude) {
            this.minLatitude = minLatitude;
            this.maxLatitude = maxLatitude;
            this.minLongitude = minLongitude;
            this.maxLongitude = maxLongitude;
        }
    }
}
