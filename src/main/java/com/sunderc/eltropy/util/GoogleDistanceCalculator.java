package com.sunderc.eltropy.util;

import com.sunderc.eltropy.location.model.DirectionParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Utility class which calculates the distance between 2 given coordinates on the maps.
 *
 * Uses Google API
 */
@Service
public class GoogleDistanceCalculator {

    public static final Logger log = LoggerFactory.getLogger(GoogleDistanceCalculator.class);

    public static final String DESTINATION_ADDRESS = "destination_addresses";
    public static final String ORIGIN_ADDRESS = "origin_addresses";
    public static final String ROWS = "rows";
    public static final String ELEMENTS = "elements";

    @Value("${google.maps.apikey:AIzaSyD6yTpaKwHV8aQ0hWmYRLC1G6iogvB0Vv0}")
    private String googleApiKey;

    private final ObjectMapper mapper = new ObjectMapper();
    private final NumberFormat numberFormat = NumberFormat.getInstance();

    private String mapsUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?";
    private String params="key=%s&origins=%s&destinations=%s&sensor=false&units=metric&mode=driving";

    public DirectionParams calculateDistanceAndAddresses(DirectionParams directionParams) throws IOException{
        try (CloseableHttpClient client = HttpClients.createDefault()) {

            String finalUrl = mapsUrl+String.format(params,googleApiKey,directionParams.getStartingLatitude()+","+
                    directionParams.getStartingLongitude(),directionParams.getDestinationLatitude()+","+
                    directionParams.getDestinationLongitude());

            log.debug("The URL to be queried with Google API is:"+finalUrl);

            HttpGet request = new HttpGet(finalUrl);

            Map response = client.execute(request, httpResponse ->
                    mapper.readValue(httpResponse.getEntity().getContent(), Map.class));
            parseValuesAndSet(response,directionParams);
        }

        return directionParams;
    }

    private void parseValuesAndSet(Map response,DirectionParams params){
        List<String> destinationAddresses = (List<String>)response.get(DESTINATION_ADDRESS);
        List<String> originAddresses = (List<String>)response.get(ORIGIN_ADDRESS);
        if(destinationAddresses!=null && destinationAddresses.size()>0){
            params.setDestinationAddress(destinationAddresses.get(0));
            log.debug("Destination Address:"+params.getDestinationAddress());
        }
        if(originAddresses!=null && originAddresses.size()>0){
            params.setStartingAddress(originAddresses.get(0));
            log.debug("Origin or Starting Address:"+params.getStartingAddress());
        }

        List<Map> rows = (List<Map>)response.get(ROWS);
        if(rows!=null && rows.size()>0){
            List<Map> elements = (List<Map>)((Map) rows.get(0)).get(ELEMENTS);
            if(elements!=null && elements.size()>0){
                Map distanceDurations = (Map)elements.get(0);
                String distance = ((Map<String,String>)distanceDurations.get("distance")).get("text");
                if(distance!=null) {
                    try {
                        Number num = numberFormat.parse(distance.split(" ")[0]);
                        params.setDistanceInKms(num.intValue());
                    }catch(ParseException pe){
                        log.error("Parse exception has occured.",pe);
                    }
                }
            }
        }
    }
}
