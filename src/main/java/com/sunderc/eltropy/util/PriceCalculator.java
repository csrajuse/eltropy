package com.sunderc.eltropy.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Calculates the price of the ride based on the distance and some rules.
 */
@Service
public class PriceCalculator {

    @Value("${price.per.km}")
    private String pricePerKms;
    @Value("${price.booking.fee}")
    private String bookingFee;
    @Value("${percentage.cgst}")
    private String percentageCgst;
    @Value("${percentage.sgst}")
    private String percentageSgst;

    private static final DecimalFormat df = new DecimalFormat();

    public Float calculatePrice(Integer distance){
        df.setMaximumFractionDigits(2);
        float pricePerKmsInt = Float.parseFloat(pricePerKms);
        Float basePrice = distance*pricePerKmsInt;
        Float bookingPrice = Float.parseFloat(bookingFee);
        Float cgstPrice = Float.parseFloat(percentageCgst)*basePrice;
        Float sgstPrice = Float.parseFloat(percentageSgst)*basePrice;

        return basePrice+bookingPrice+cgstPrice+sgstPrice;
    }
}
