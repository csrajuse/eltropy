package com.sunderc.eltropy.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Since we are using jwt functionality and passing in token. When there is a logout
 * we need to make sure that logout works. For that we store the tokens that have been logged
 * and when the token expiration has already occurred then we remove from this list
 *
 * By default spring treats services as singleton so we do not need to specifically create a singleton class here.
 */
@Service
public class TokenBlacklistStore {
    @Autowired
    private JwtUtil jwtUtil;

    private final List<String> tokenStore = new ArrayList<>();

    public void addToTokenStore(String token){
        tokenStore.add(token);
    }

    public boolean isLoggedOut(String token){
        if(tokenStore.contains(token)) return true;
        return false;
    }

    @Scheduled(fixedRate=1000*60*60*10)
    public void removeStaleTokens(){
        tokenStore.forEach(token->{
            if(jwtUtil.isTokenExpired(token)){
                tokenStore.remove(token);
            }
        });
    }
}
