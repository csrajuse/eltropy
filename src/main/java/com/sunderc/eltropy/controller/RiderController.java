package com.sunderc.eltropy.controller;

import com.sunderc.eltropy.location.model.Coordinates;
import com.sunderc.eltropy.ride.model.RideAcceptRequest;
import com.sunderc.eltropy.ride.model.RideRequest;
import com.sunderc.eltropy.services.RideService;
import com.sunderc.eltropy.services.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("rider")
public class RiderController {

    private static final Logger log = LoggerFactory.getLogger(RiderController.class);

    @Autowired
    private RideService rideService;

    @RequestMapping(value="/createRide" , method = RequestMethod.POST)
    public ResponseEntity<?> createANewRide(@RequestBody RideRequest rideRequest){
        Coordinates originCoordinates = new Coordinates(rideRequest.getStartLatitude(),rideRequest.getStartLongitude());
        Coordinates destCoordinates = new Coordinates(rideRequest.getDestLatitude(),rideRequest.getDestLongitude());
        try {
            UUID rideId =rideService.bookARide(rideRequest.getUserId(), originCoordinates, destCoordinates);
            return ResponseEntity.ok(rideId.toString());
        }catch(ServiceException se){
            log.error("Exception occured booking a ride",se);
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).build();
        }
    }

    @RequestMapping(value="/cancelRide",method=RequestMethod.POST)
    public ResponseEntity<?> cancelRide(@RequestBody RideAcceptRequest rideAcceptRequest){
        try {
            String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
            rideService.cancelRideByRider(UUID.fromString(rideAcceptRequest.getRideId()), username);
            return ResponseEntity.ok().build();
        }catch(ServiceException se){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(se.getMessage());
        }
    }
}
