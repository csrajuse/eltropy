package com.sunderc.eltropy.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("logout")
public class LogoutController {

    @RequestMapping(value="/logout" , method = RequestMethod.POST)
    public ResponseEntity<?> logout(){
        Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
        System.out.println(""+details);
        return ResponseEntity.ok().build();
    }

}
