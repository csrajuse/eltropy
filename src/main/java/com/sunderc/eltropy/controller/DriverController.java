package com.sunderc.eltropy.controller;

import com.sunderc.eltropy.ride.model.PotentialRideResponse;
import com.sunderc.eltropy.ride.model.RideAcceptRequest;
import com.sunderc.eltropy.services.DriverService;
import com.sunderc.eltropy.services.RideService;
import com.sunderc.eltropy.services.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("driver")
public class DriverController {

    @Autowired
    private DriverService driverService;
    @Autowired
    private RideService rideService;

    @RequestMapping(value="/getRides/{id}",method= RequestMethod.GET)
    public ResponseEntity<?> getRidesForTheDriver(@PathVariable Long id){
        List<PotentialRideResponse> rides = driverService.findPotentialRidesForUser(id);

        return ResponseEntity.ok(rides);
    }

    @RequestMapping(value="/getBestRide/{id}",method= RequestMethod.GET)
    public ResponseEntity<?> getBestRides(@PathVariable Long id){
        List<PotentialRideResponse> rides = driverService.findPotentialRidesForUser(id);

        if(rides.size()>0)
            return ResponseEntity.ok(rides.get(0));
        else{
            return ResponseEntity.ok("");
        }
    }

    @RequestMapping(value="/acceptRide",method= RequestMethod.POST)
    public ResponseEntity<?> acceptRide(@RequestBody RideAcceptRequest rideAcceptRequest){
        try {
            String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
            rideService.acceptRide(UUID.fromString(rideAcceptRequest.getRideId()), username);
            return ResponseEntity.ok().build();
        }catch(ServiceException se){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(se.getMessage());
        }
    }

    @RequestMapping(value="/cancelRide",method=RequestMethod.POST)
    public ResponseEntity<?> cancelRide(@RequestBody RideAcceptRequest rideAcceptRequest){
        try {
            String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
            rideService.cancelRideByDriver(UUID.fromString(rideAcceptRequest.getRideId()), username);
            return ResponseEntity.ok().build();
        }catch(ServiceException se){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(se.getMessage());
        }
    }
}
