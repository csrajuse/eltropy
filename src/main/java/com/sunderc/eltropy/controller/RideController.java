package com.sunderc.eltropy.controller;

import com.sunderc.eltropy.ride.model.RideLocationRequest;
import com.sunderc.eltropy.ride.model.RideRequest;
import com.sunderc.eltropy.services.RideService;
import com.sunderc.eltropy.services.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("ride")
public class RideController {

    @Autowired
    private RideService rideService;

    @RequestMapping(value="/updateRideLocation" , method = RequestMethod.POST)
    public ResponseEntity<?> createANewRide(@RequestBody RideLocationRequest request){
        try {
            rideService.updateRideLocation(UUID.fromString(request.getRideId()),request.getLatitude(),request.getLongitude());
            return ResponseEntity.ok().build();
        }catch(ServiceException se){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(se.getMessage());
        }
    }
}
