package com.sunderc.eltropy.db.service;

import com.sunderc.eltropy.db.model.PotentialRide;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PotentialRideDAOService extends CrudRepository<PotentialRide, UUID> {

    @Query("select pr.id from PotentialRide pr where pr.ride=?1 and pr.forDriver=?2")
    List<UUID> findRidesForRideAndDriver(UUID rideId, Long driverId);

    @Query("select pr from PotentialRide pr where pr.forDriver=?1")
    List<PotentialRide> findRidesForDriver(Long driverId);

    @Query("select pr from PotentialRide pr where pr.ride=?1")
    List<PotentialRide> findRidesForRide(UUID rideId);

    @Query("delete from PotentialRide pr where pr.ride=?1")
    int deleteByRideId(UUID rideId);
}
