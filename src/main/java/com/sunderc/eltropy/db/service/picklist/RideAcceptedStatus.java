package com.sunderc.eltropy.db.service.picklist;

public interface RideAcceptedStatus {

    public static final String ACTIVE = "active";
    public static final String CANCELED = "cancel";
    public static final String FINISHED = "finished";

}
