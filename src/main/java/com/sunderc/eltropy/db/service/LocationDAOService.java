package com.sunderc.eltropy.db.service;

import com.sunderc.eltropy.db.model.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LocationDAOService extends CrudRepository<Location, UUID> {
}
