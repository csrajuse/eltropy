package com.sunderc.eltropy.db.service;

import com.sunderc.eltropy.db.model.OngoingRideLocation;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface OngoingRideLocationDAOService extends CrudRepository<OngoingRideLocation, UUID> {
}
