package com.sunderc.eltropy.db.service;

import com.sunderc.eltropy.db.model.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleDAOService extends CrudRepository<Role,Long> {

}
