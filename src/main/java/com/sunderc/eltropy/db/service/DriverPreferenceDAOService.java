package com.sunderc.eltropy.db.service;

import com.sunderc.eltropy.db.model.DriverPreference;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DriverPreferenceDAOService extends CrudRepository<DriverPreference,Long> {

    @Query("select dp from DriverPreference dp where dp.driver=?1 and dp.name=?2")
    List<DriverPreference> findDriverPreferenceForDriver(Long userId, String name);
}
