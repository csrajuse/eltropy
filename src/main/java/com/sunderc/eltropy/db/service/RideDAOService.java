package com.sunderc.eltropy.db.service;

import com.sunderc.eltropy.db.model.Ride;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RideDAOService extends CrudRepository<Ride, UUID> {

    @Query("select r from Ride r where r.userRequested=?1")
    List<Ride> findAllRidesByUserId(Long userId);

    @Query("select r from Ride r where r.status=?1")
    List<Ride> findAllRidesByStatus(String status);

    @Query("select r from Ride r where r.status=?1 and r.id=?2")
    List<Ride> findRideByStatusAndId(String status,UUID rideId);

    @Query("select r from Ride r, Location l where r.location=l.id and l.latitude>?1 and l.longitude>?2" +
            " and l.latitude<?3 and l.longitude<?4 and r.status=?5")
    List<Ride> findAllRidesInsideTheLocationConstaints(Float minLatitude,Float minLongitude,Float maxLatitude,Float maxLongitude,String status);
}
