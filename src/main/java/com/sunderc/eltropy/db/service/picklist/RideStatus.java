package com.sunderc.eltropy.db.service.picklist;

public interface RideStatus {
    public static final String NEW = "new";
    public static final String TAKEN = "taken";
    public static final String CANCELED = "cancel";
    public static final String FINISHED = "finished";
}
