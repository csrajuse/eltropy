package com.sunderc.eltropy.db.service;

/**
 * Interface implementing the default CrudRepository from Spring JPA implementation.
 *
 * Expected outcome class is User.
 */

import com.sunderc.eltropy.db.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDAOService extends CrudRepository<User,Long> {

    @Query("select u from User u where u.username=?1")
    List<User> findByUsername(String userName);

    @Query("select u from User u where u.role=1 and u.active=true")
    List<User> findAllDriversThatAraActive();
}
