package com.sunderc.eltropy.db.service;

import com.sunderc.eltropy.db.model.RideAccepted;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RideAcceptedDAOService extends CrudRepository<RideAccepted, UUID> {

    @Query("select ra from RideAccepted ra where ra.status=?1")
    List<RideAccepted> findRidesAcceptedByStatus(String status);

    @Query("select ra from RideAccepted ra where ra.status=?1 and ra.driver=?2")
    List<RideAccepted> findRidesByStatusAndDriver(String status,Long driver);

    @Query("select ra from RideAccepted ra where ra.status=?1 and ra.ride=?2")
    List<RideAccepted> findRidesByStatusAndRide(String status,UUID ride);
}
