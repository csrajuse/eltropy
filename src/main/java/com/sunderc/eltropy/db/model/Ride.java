package com.sunderc.eltropy.db.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="rides")
public class Ride {

    @Id
    @Type(type="pg-uuid")
    private UUID id;
    @Column(name="time_requested")
    private LocalDateTime timeRequested;
    @Column(name="user_requested")
    private Long userRequested;
    @Type(type="pg-uuid")
    private UUID location;
    @Type(type="pg-uuid")
    @Column(name="to_location")
    private UUID toLocation;
    private Integer distance;
    private Float price;
    private String status;

    public Ride() {
    }

    public Ride(LocalDateTime timeRequested, Long userRequested, UUID location, UUID toLocation,String status) {
        this.timeRequested = timeRequested;
        this.userRequested = userRequested;
        this.location = location;
        this.toLocation = toLocation;
        this.status = status;
        this.id = UUID.randomUUID();
    }

    public Ride(UUID id, LocalDateTime timeRequested, Long userRequested, UUID location, UUID toLocation, Integer distance, Float price, String status) {
        this.id = id;
        this.timeRequested = timeRequested;
        this.userRequested = userRequested;
        this.location = location;
        this.toLocation = toLocation;
        this.distance = distance;
        this.price = price;
        this.status = status;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getTimeRequested() {
        return timeRequested;
    }

    public void setTimeRequested(LocalDateTime timeRequested) {
        this.timeRequested = timeRequested;
    }

    public Long getUserRequested() {
        return userRequested;
    }

    public void setUserRequested(Long userRequested) {
        this.userRequested = userRequested;
    }

    public UUID getLocation() {
        return location;
    }

    public void setLocation(UUID location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UUID getToLocation() {
        return toLocation;
    }

    public void setToLocation(UUID toLocation) {
        this.toLocation = toLocation;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
