package com.sunderc.eltropy.db.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="potential_rides")
public class PotentialRide {
    @Id
    @Type(type="pg-uuid")
    private UUID id;
    @Type(type="pg-uuid")
    private UUID ride;
    @Column(name="for_driver")
    private Long forDriver;
    @Column(name="created_at")
    private LocalDateTime createdAt;
    private Boolean active;

    public PotentialRide() {
    }

    public PotentialRide(UUID ride, Long forDriver, LocalDateTime createdAt, Boolean active) {
        this.ride = ride;
        this.forDriver = forDriver;
        this.createdAt = createdAt;
        this.active = active;
        this.id=UUID.randomUUID();
    }

    public PotentialRide(UUID id, UUID ride, Long forDriver, LocalDateTime createdAt, Boolean active) {
        this.id = id;
        this.ride = ride;
        this.forDriver = forDriver;
        this.createdAt = createdAt;
        this.active = active;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getRide() {
        return ride;
    }

    public void setRide(UUID ride) {
        this.ride = ride;
    }

    public Long getForDriver() {
        return forDriver;
    }

    public void setForDriver(Long forDriver) {
        this.forDriver = forDriver;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
