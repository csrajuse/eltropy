package com.sunderc.eltropy.db.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="rides_accepted")
public class RideAccepted {

    @Id
    @Type(type="pg-uuid")
    private UUID id;
    @Column(name="time_accepted")
    private LocalDateTime timeAccepted;
    @Column(name="driver_user")
    private Long driver;
    private UUID ride;
    @Column(name="at_location")
    @Type(type="pg-uuid")
    private UUID atLocation;
    private String status;

    public RideAccepted() {
    }

    public RideAccepted(LocalDateTime timeAccepted, UUID ride,Long driver, UUID atLocation, String status) {
        this.timeAccepted = timeAccepted;
        this.ride=ride;
        this.driver = driver;
        this.atLocation = atLocation;
        this.status = status;
        this.id=UUID.randomUUID();
    }

    public RideAccepted(UUID id, LocalDateTime timeAccepted,UUID ride, Long driver, UUID atLocation, String status) {
        this.id = id;
        this.timeAccepted = timeAccepted;
        this.ride=ride;
        this.driver = driver;
        this.atLocation = atLocation;
        this.status = status;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getTimeAccepted() {
        return timeAccepted;
    }

    public void setTimeAccepted(LocalDateTime timeAccepted) {
        this.timeAccepted = timeAccepted;
    }

    public Long getDriver() {
        return driver;
    }

    public void setDriver(Long driver) {
        this.driver = driver;
    }

    public UUID getAtLocation() {
        return atLocation;
    }

    public void setAtLocation(UUID atLocation) {
        this.atLocation = atLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
