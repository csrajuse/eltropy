package com.sunderc.eltropy.db.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="ongoing_ride_location")
public class OngoingRideLocation {

    @Id
    @Type(type="pg-uuid")
    private UUID id;
    @Column(name="accepted_ride")
    private UUID acceptedRide;
    @Column(name="at_location")
    private UUID atLocation;
    @Column(name="created_at")
    private LocalDateTime createdAt;

    public OngoingRideLocation() {
    }

    public OngoingRideLocation(UUID acceptedRide, UUID atLocation, LocalDateTime createdAt) {
        this.acceptedRide = acceptedRide;
        this.atLocation = atLocation;
        this.createdAt = createdAt;
        this.id=UUID.randomUUID();
    }

    public OngoingRideLocation(UUID id, UUID acceptedRide, UUID atLocation, LocalDateTime createdAt) {
        this.id = id;
        this.acceptedRide = acceptedRide;
        this.atLocation = atLocation;
        this.createdAt = createdAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAcceptedRide() {
        return acceptedRide;
    }

    public void setAcceptedRide(UUID acceptedRide) {
        this.acceptedRide = acceptedRide;
    }

    public UUID getAtLocation() {
        return atLocation;
    }

    public void setAtLocation(UUID atLocation) {
        this.atLocation = atLocation;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
