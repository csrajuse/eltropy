package com.sunderc.eltropy.db.model;

import javax.persistence.*;

@Entity
@Table(name="driver_preferences")
public class DriverPreference {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long driver;
    private String name;
    private String value;

    public DriverPreference() {
    }

    public DriverPreference(Long driver, String name, String value) {
        this.driver = driver;
        this.name = name;
        this.value = value;
    }

    public DriverPreference(Long id, Long driver, String name, String value) {
        this.id = id;
        this.driver = driver;
        this.name = name;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDriver() {
        return driver;
    }

    public void setDriver(Long driver) {
        this.driver = driver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
