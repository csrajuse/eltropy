package com.sunderc.eltropy.db.model;

import org.hibernate.annotations.Type;

import java.util.UUID;
import javax.persistence.*;

@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;
    private Long role;
    @Type(type="pg-uuid")
    @Column(name="current_location")
    private UUID currentLocation;
    private Boolean active;
    private Boolean deleted;
    private String username;
    private String password;

    public User() {
    }

    public User(String firstName, String lastName, Long role, UUID currentLocation, Boolean active, Boolean deleted, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.currentLocation = currentLocation;
        this.active = active;
        this.deleted = deleted;
        this.username = username;
        this.password = password;
    }

    public User(Long id, String firstName, String lastName, Long role, UUID currentLocation, Boolean active, Boolean deleted, String username, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.currentLocation = currentLocation;
        this.active = active;
        this.deleted = deleted;
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getRole() {
        return role;
    }

    public void setRole(Long role) {
        this.role = role;
    }

    public UUID getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(UUID currentLocation) {
        this.currentLocation = currentLocation;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
