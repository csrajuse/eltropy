package com.sunderc.eltropy.location.model;

/**
 * Simple POJO which contains Latitude and Longitude.
 */
public class Coordinates {

    private Float longitude;
    private Float latitude;

    public Coordinates(Float latitude, Float longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }
}
