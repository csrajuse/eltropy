package com.sunderc.eltropy.location.model;

/**
 * Holds information like
 * Starting latitude and longitude
 * Starting address
 * Destination latitude and longitude
 * Destination address
 * Distance in Kms
 */
public class DirectionParams {

    private Float startingLatitude;
    private Float startingLongitude;
    private Float destinationLatitude;
    private Float destinationLongitude;
    private String startingAddress;
    private String destinationAddress;
    private Integer distanceInKms;


    public DirectionParams(Float startingLatitude, Float startingLongitude, Float destinationLatitude, Float destinationLongitude) {
        this.startingLatitude = startingLatitude;
        this.startingLongitude = startingLongitude;
        this.destinationLatitude = destinationLatitude;
        this.destinationLongitude = destinationLongitude;
    }

    public Float getStartingLatitude() {
        return startingLatitude;
    }

    public void setStartingLatitude(Float startingLatitude) {
        this.startingLatitude = startingLatitude;
    }

    public Float getStartingLongitude() {
        return startingLongitude;
    }

    public void setStartingLongitude(Float startingLongitude) {
        this.startingLongitude = startingLongitude;
    }

    public Float getDestinationLatitude() {
        return destinationLatitude;
    }

    public void setDestinationLatitude(Float destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    public Float getDestinationLongitude() {
        return destinationLongitude;
    }

    public void setDestinationLongitude(Float destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }

    public String getStartingAddress() {
        return startingAddress;
    }

    public void setStartingAddress(String startingAddress) {
        this.startingAddress = startingAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public Integer getDistanceInKms() {
        return distanceInKms;
    }

    public void setDistanceInKms(Integer distanceInKms) {
        this.distanceInKms = distanceInKms;
    }
}
