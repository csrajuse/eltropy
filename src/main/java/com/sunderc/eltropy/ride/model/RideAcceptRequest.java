package com.sunderc.eltropy.ride.model;

public class RideAcceptRequest {
    private String rideId;

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }
}
