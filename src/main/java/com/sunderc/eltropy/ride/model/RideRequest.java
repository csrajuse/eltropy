package com.sunderc.eltropy.ride.model;

public class RideRequest {
    private Long userId;
    private Float startLatitude;
    private Float startLongitude;
    private Float destLatitude;
    private Float destLongitude;

    public RideRequest() {
    }

    public RideRequest(Long userId, Float startLatitude, Float startLongitude, Float destLatitude, Float destLongitude) {
        this.userId = userId;
        this.startLatitude = startLatitude;
        this.startLongitude = startLongitude;
        this.destLatitude = destLatitude;
        this.destLongitude = destLongitude;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Float getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(Float startLatitude) {
        this.startLatitude = startLatitude;
    }

    public Float getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(Float startLongitude) {
        this.startLongitude = startLongitude;
    }

    public Float getDestLatitude() {
        return destLatitude;
    }

    public void setDestLatitude(Float destLatitude) {
        this.destLatitude = destLatitude;
    }

    public Float getDestLongitude() {
        return destLongitude;
    }

    public void setDestLongitude(Float destLongitude) {
        this.destLongitude = destLongitude;
    }
}
