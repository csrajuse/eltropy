package com.sunderc.eltropy.ride.model;

public class PotentialRideResponse {
    private String rideId;
    private String riderFirstName;
    private String riderLastName;
    private String riderAddress;
    private Integer distance;

    public PotentialRideResponse() {
    }

    public PotentialRideResponse(String rideId, String riderFirstName, String riderLastName, String riderAddress, Integer distance) {
        this.rideId = rideId;
        this.riderFirstName = riderFirstName;
        this.riderLastName = riderLastName;
        this.riderAddress = riderAddress;
        this.distance = distance;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getRiderFirstName() {
        return riderFirstName;
    }

    public void setRiderFirstName(String riderFirstName) {
        this.riderFirstName = riderFirstName;
    }

    public String getRiderLastName() {
        return riderLastName;
    }

    public void setRiderLastName(String riderLastName) {
        this.riderLastName = riderLastName;
    }

    public String getRiderAddress() {
        return riderAddress;
    }

    public void setRiderAddress(String riderAddress) {
        this.riderAddress = riderAddress;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
}
