package com.sunderc.eltropy.filter;

import com.sunderc.eltropy.services.UserService;
import com.sunderc.eltropy.util.JwtUtil;
import com.sunderc.eltropy.util.TokenBlacklistStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private UserService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TokenBlacklistStore tokenBlacklistStore;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        final String authorizationHeader = httpServletRequest.getHeader("Authorization");
        String username = null;
        String jwt = null;

        if(!Objects.isNull(authorizationHeader) && authorizationHeader.startsWith("Bearer ")){
            jwt = authorizationHeader.substring(7);
            username = jwtUtil.extractUsername(jwt);
        }

        if(Objects.nonNull(username) && SecurityContextHolder.getContext().getAuthentication() == null){
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            boolean correctRole = false;
            if(httpServletRequest.getRequestURI().startsWith("/driver")){
                correctRole = userDetails.getAuthorities().iterator().next().getAuthority().equals("driver");
            }
            else if(httpServletRequest.getRequestURI().startsWith("/rider")){
                correctRole = userDetails.getAuthorities().iterator().next().getAuthority().equals("rider");
            }
            else if(httpServletRequest.getRequestURI().startsWith("/logout")){
                correctRole = true;
            }
            if(jwtUtil.validateToken(jwt,userDetails) && correctRole && !tokenBlacklistStore.isLoggedOut(jwt)){
                if(httpServletRequest.getRequestURI().startsWith("/logout")){
                    tokenBlacklistStore.addToTokenStore(jwt);
                }
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));

                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(httpServletRequest,httpServletResponse);
    }
}
