package com.sunderc.eltropy.services;

import com.sunderc.eltropy.db.model.*;
import com.sunderc.eltropy.db.service.*;
import com.sunderc.eltropy.db.service.picklist.RideAcceptedStatus;
import com.sunderc.eltropy.db.service.picklist.RideStatus;
import com.sunderc.eltropy.ride.model.PotentialRideResponse;
import com.sunderc.eltropy.util.LatitudeLongitudeCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DriverService {

    @Autowired
    private PotentialRideDAOService potentialRideDAOService;
    @Autowired
    private RideAcceptedDAOService rideAcceptedDAOService;
    @Autowired
    private RideDAOService rideDAOService;
    @Autowired
    private UserDAOService userDAOService;
    @Autowired
    private DriverPreferenceDAOService driverPreferenceDAOService;
    @Autowired
    private LocationDAOService locationDAOService;

    //For simplicity hard coding the role id of a driver.
    private static final Long DRIVE_ROLE = 2L;

    private static final Logger log = LoggerFactory.getLogger(DriverService.class);

    private static final String DISTANCE_PREFERENCE = "distance";

    /**
     * Scheduled to run every 5 seconds. This job will load all the drivers and search for the rides
     * that can be added potentially to them. The rides will be added to the Driver only if they are
     * not associated with an active ride.
     *
     * This will NOT scale if the data is a lot. One way of scaling this is to have many microservices and
     * they process rides as a batch. For now I will take all the rides that are there in the new state
     * and add them as potential rides for the drivers who are in ope status.
     *
     * The way to determine if the Driver is active is true. He needs to be in active mode (from users table)
     * and should not have any rides accepted at the time this code runs.
     */
    @Scheduled(fixedRate=5*1000)
    public void configureRidesForUsers(){
        log.info("Started Configuring rides for Users at:"+ LocalDateTime.now());
        //Get All the Drivers who have accepted the Rides.
        List<RideAccepted> ridesAccepted = rideAcceptedDAOService.findRidesAcceptedByStatus(RideAcceptedStatus.ACTIVE);
        //Get all the Drivers who are not in rideAcceptedState
        List<User> driversLookingForRide = userDAOService.findAllDriversThatAraActive();
        driversLookingForRide.stream().filter(user->!ridesAccepted.stream().map(RideAccepted::getDriver)
                        .collect(Collectors.toList()).contains(user.getId())).
        forEach(user->{
            //Find the driver preference setting.Ideally this will be in a Cache.
            List<DriverPreference> driverPreferences = driverPreferenceDAOService.findDriverPreferenceForDriver(user.getId(),
                    DISTANCE_PREFERENCE);
            int distance = driverPreferences!=null && driverPreferences.size()>0 ? Integer.parseInt(driverPreferences.get(0).getValue()) :
                    500;

            log.info("For driver with username:"+user.getUsername()+" the distance prefrence is :"+distance);
            Optional<Location> userCurrentLocation = locationDAOService.findById(user.getCurrentLocation());
            //Find the maximum and minimum latitudes and longitudes for the driver preference.
            userCurrentLocation.ifPresent(currLocation->{
                LatitudeLongitudeCalculator.MaxMinLatAndLong minMaxLatAndLong = LatitudeLongitudeCalculator.calculateMaxMinLatAndLongForIndia(
                        distance,currLocation.getLatitude(),currLocation.getLongitude()
                );
                List<Ride> newRides = rideDAOService.findAllRidesInsideTheLocationConstaints(minMaxLatAndLong.minLatitude,
                        minMaxLatAndLong.minLongitude,
                        minMaxLatAndLong.maxLatitude,
                        minMaxLatAndLong.maxLongitude,
                        RideStatus.NEW);

                newRides.
                forEach(newRide->{
                    if(potentialRideDAOService.findRidesForRideAndDriver(newRide.getId(),user.getId()).size() == 0) {
                        PotentialRide ride = new PotentialRide(newRide.getId(), user.getId(), LocalDateTime.now(), true);
                        potentialRideDAOService.save(ride);
                    }
                });
            });
        });
    }

    public List<PotentialRideResponse> findPotentialRidesForUser(Long userId){
            List<PotentialRide> potentialRides = potentialRideDAOService.findRidesForDriver(userId);
        List<PotentialRideResponse> ridesResponse = new ArrayList<>();
        potentialRides.forEach(potentialRide -> {
            Optional<Ride> ride = rideDAOService.findById(potentialRide.getRide());
            ride.ifPresent(rideObject->{
                PotentialRideResponse response = new PotentialRideResponse();
                response.setRideId(rideObject.getId().toString());
                Optional<Location> startLocation = locationDAOService.findById(rideObject.getLocation());
                Optional<Location> endLocation = locationDAOService.findById(rideObject.getToLocation());
                startLocation.ifPresent(location -> response.setRiderAddress(location.getAddress()));
                if(startLocation.isPresent() && endLocation.isPresent()){
                    double distance = LatitudeLongitudeCalculator.calculateDistanceBetweenCoordinates(startLocation.get().getLatitude(),
                            startLocation.get().getLongitude(),
                            endLocation.get().getLatitude(),
                            endLocation.get().getLongitude());

                    response.setDistance((int)distance);
                    Optional<User> user = userDAOService.findById(rideObject.getUserRequested());
                    user.ifPresent(userObj->{
                        response.setRiderFirstName(userObj.getFirstName());
                        response.setRiderLastName(userObj.getLastName());
                        ridesResponse.add(response);
                    });
                }
            });
        });

        ridesResponse.sort(new Comparator<PotentialRideResponse>() {
            @Override
            public int compare(PotentialRideResponse o1, PotentialRideResponse o2) {
                return o1.getDistance()-o2.getDistance();
            }
        });
        return ridesResponse;
    }
}
