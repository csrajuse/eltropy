package com.sunderc.eltropy.services;

import com.sunderc.eltropy.db.model.*;
import com.sunderc.eltropy.db.service.*;
import com.sunderc.eltropy.db.service.picklist.RideAcceptedStatus;
import com.sunderc.eltropy.db.service.picklist.RideStatus;
import com.sunderc.eltropy.location.model.Coordinates;
import com.sunderc.eltropy.location.model.DirectionParams;
import com.sunderc.eltropy.util.GoogleDistanceCalculator;
import com.sunderc.eltropy.util.LatitudeLongitudeCalculator;
import com.sunderc.eltropy.util.PriceCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Business logic layer: Ride Service which encompasses all the logic related to the ride.
 */

@Service
public class RideService {

    private static final Logger log = LoggerFactory.getLogger(RideService.class);

    @Autowired
    private RideDAOService rideDAOService;
    @Autowired
    private LocationDAOService locationDAOService;
    @Autowired
    private GoogleDistanceCalculator googleDistanceCalculator;
    @Autowired
    private PriceCalculator priceCalculator;
    @Autowired
    private RideAcceptedDAOService rideAcceptedDAOService;
    @Autowired
    private UserDAOService userDAOService;
    @Autowired
    private PotentialRideDAOService potentialRideDAOService;
    @Autowired
    private OngoingRideLocationDAOService ongoingRideLocationDAOService;

    /**
     *
     * @param userId Id of the user who is requesting the ride
     * @param originCoordinates
     * @param destCoordinates
     * @throws ServiceException
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public UUID bookARide(Long userId, Coordinates originCoordinates,Coordinates destCoordinates)
            throws ServiceException{
        if(LatitudeLongitudeCalculator.validateIfLocationValuesAreInIndia(originCoordinates.getLatitude(),originCoordinates.getLongitude())
        && LatitudeLongitudeCalculator.validateIfLocationValuesAreInIndia(destCoordinates.getLatitude(),destCoordinates.getLongitude())){
            Location originLocation = new Location(originCoordinates.getLatitude(),originCoordinates.getLongitude(),null, LocalDateTime.now());
            Location destLocation = new Location(destCoordinates.getLatitude(),destCoordinates.getLongitude(),null, LocalDateTime.now());
            UUID originLocationId = locationDAOService.save(originLocation).getId();
            UUID destLocationId = locationDAOService.save(destLocation).getId();

            Ride ride = new Ride(LocalDateTime.now(),userId,originLocationId,destLocationId, RideStatus.NEW);
            ride = rideDAOService.save(ride);

            calculateRideParams(ride,originLocation,destLocation);

            return ride.getId();

        }else{
            log.error("The Coordinates of Origin or Destination are not valid");
            throw new ServiceException("The Coordinates of Origin or Destination are not valid");
        }
    }

    /**
     *
     * @param ride
     * @param originLocation
     * @param destLocation
     *
     * TODO: put it in a microservice which can be listening to a message on a topic.
     */
    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void calculateRideParams(Ride ride,Location originLocation,Location destLocation){
        try {
            DirectionParams params = new DirectionParams(originLocation.getLatitude(), originLocation.getLongitude(),
                    destLocation.getLatitude(), destLocation.getLongitude());
            params = googleDistanceCalculator.calculateDistanceAndAddresses(params);
            originLocation.setAddress(params.getStartingAddress());
            destLocation.setAddress((params.getDestinationAddress()));
            Float priceForRide = priceCalculator.calculatePrice(params.getDistanceInKms());
            ride.setDistance(params.getDistanceInKms());
            ride.setPrice(priceForRide);

            locationDAOService.save(originLocation);
            locationDAOService.save(destLocation);
            rideDAOService.save(ride);

        }catch(IOException ioe){
            log.error("Error occurred when calculating ride parameters.",ioe);
        }
    }

    /**
     *
     * @param rideId Id of the ride
     * @param username of the driver.
     */
    @Transactional
    public void acceptRide(UUID rideId,String username) throws ServiceException{
        List<User> users = userDAOService.findByUsername(username);
        if(users.size()>0) {
            //Check if the driver is already going through a ride, then throw an Exception
            Long userId = users.get(0).getId();
            if (rideAcceptedDAOService.findRidesByStatusAndRide(RideAcceptedStatus.ACTIVE,rideId).size() > 0) {
                log.error("The ride is already taken. Please select another ride.");
                throw new ServiceException("The ride is already taken. Please select another ride.");
            }
            //Check if the ride is still in new status
            List<Ride> rideAvailable = rideDAOService.findRideByStatusAndId(RideStatus.NEW, rideId);
            if (rideAvailable.size() > 0) {
                Ride ride = rideAvailable.get(0);
                //Set the ride to RideStatus.
                ride.setStatus(RideStatus.TAKEN);
                rideDAOService.save(ride);
                //LocalDateTime timeAccepted, Long driver, UUID atLocation, String status
                RideAccepted rideAccepted = new RideAccepted(LocalDateTime.now(),rideId, userId, ride.getLocation(), RideAcceptedStatus.ACTIVE);

                rideAcceptedDAOService.save(rideAccepted);
                List<PotentialRide> potentialRidesForUser = potentialRideDAOService.findRidesForDriver(userId);
                List<PotentialRide> potentialRidesForRideId = potentialRideDAOService.findRidesForRide(rideId);
                potentialRidesForUser.addAll(potentialRidesForRideId);
                //TODO: this needs to change as it is deleting one object at a time.
                //Investigate why list delete is not working.
                potentialRidesForUser.forEach(potentialRide -> potentialRideDAOService.delete(potentialRide));
            } else {
                log.error("The ride is not available.");
                throw new ServiceException("The ride is not available.");
            }
        }else{
            log.error("User with username :"+username+" not found");
            throw new ServiceException("User with username :"+username+" not found");
        }
    }

    @Transactional
    public void cancelRideByDriver(UUID rideId,String username) throws ServiceException{
        List<User> users = userDAOService.findByUsername(username);
        if(users.size()>0) {
            //Check if the driver is already going through a ride, then throw an Exception
            Long userId = users.get(0).getId();
            List<RideAccepted> activeRides = rideAcceptedDAOService.findRidesByStatusAndDriver(RideAcceptedStatus.ACTIVE,userId);
            if (activeRides.size() > 0) {
                RideAccepted rideAccepted = activeRides.get(0);
                rideAccepted.setStatus(RideAcceptedStatus.CANCELED);
                rideAcceptedDAOService.save(rideAccepted);
                Optional<Ride> ride = rideDAOService.findById(rideId);
                ride.ifPresent(rideObj->{
                    rideObj.setStatus(RideStatus.CANCELED);
                    rideDAOService.save(rideObj);
                });

            } else {
                log.error("The ride is not active.");
                throw new ServiceException("The ride is not active.");
            }
        }


    }

    @Transactional
    public void cancelRideByRider(UUID rideId,String username) throws ServiceException {
        List<User> users = userDAOService.findByUsername(username);
        if (users.size() > 0) {
            //Check if the driver is already going through a ride, then throw an Exception
            List<RideAccepted> activeRides = rideAcceptedDAOService.findRidesByStatusAndRide(RideAcceptedStatus.ACTIVE, rideId);
            if (activeRides.size() > 0) {
                RideAccepted rideAccepted = activeRides.get(0);
                rideAccepted.setStatus(RideAcceptedStatus.CANCELED);
                rideAcceptedDAOService.save(rideAccepted);
                Optional<Ride> ride = rideDAOService.findById(rideId);
                ride.ifPresent(rideObj -> {
                    rideObj.setStatus(RideStatus.CANCELED);
                    rideDAOService.save(rideObj);
                });

            } else {
                log.error("The ride is not active.");
                throw new ServiceException("The ride is not active.");
            }
        }
    }

    @Transactional
    public void updateRideLocation(UUID rideId,Float latitude,Float longitude) throws ServiceException{
        List<RideAccepted> activeRides = rideAcceptedDAOService.findRidesByStatusAndRide(RideAcceptedStatus.ACTIVE, rideId);
        if (activeRides.size() > 0) {
            RideAccepted rideAccepted = activeRides.get(0);
            Location location = new Location(latitude,longitude,null,LocalDateTime.now());
            location = locationDAOService.save(location);
            OngoingRideLocation ongoingRideLocation = new OngoingRideLocation(rideAccepted.getId(),location.getId(),
                    LocalDateTime.now());
            ongoingRideLocationDAOService.save(ongoingRideLocation);

        } else {
            log.error("The ride is not active.");
            throw new ServiceException("The ride is not active.");
        }
    }
}
