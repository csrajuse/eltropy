package com.sunderc.eltropy.services;

import com.sunderc.eltropy.db.model.Role;
import com.sunderc.eltropy.db.service.RoleDAOService;
import com.sunderc.eltropy.db.service.UserDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserDAOService userDAOService;

    @Autowired
    private RoleDAOService roleDAOService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        List<com.sunderc.eltropy.db.model.User> users = userDAOService.findByUsername(userName);
        if(users!=null && users.size() ==1) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            Optional<Role> role = roleDAOService.findById(users.get(0).getRole());
            authorities.add(new SimpleGrantedAuthority(role.orElseThrow(()->new UsernameNotFoundException("Role not found for the userName:"+userName)).getName()));
            return new User(userName, users.get(0).getPassword(),authorities);

        }else{
            throw new UsernameNotFoundException("User with Username:"+userName+" not found");
        }
    }
}
