package com.sunderc.eltropy.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Provider;

public class ServiceException extends Exception{
    private static final Logger log = LoggerFactory.getLogger(ServiceException.class);
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
        log.error("Service Exception has occured",cause);
    }
    public ServiceException(String message){
        super(message);
        log.error("Service Exception has occured");
    }
}
